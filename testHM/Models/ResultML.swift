//
//  Results.swift
//  testHM
//
//  Created by Hugo Castrejon on 06/02/22.
//

import Foundation

/// Modelos principal de las búsquedas de items, este modelo ademas de los resultados lista la información del paginado
struct Search: Decodable {
    var results: [ResultML] = []
    var paging: Paging
    enum CodingKeys: String, CodingKey {
        case results
        case paging
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        paging = try values.decode(Paging.self, forKey: .paging)
        results = try values.decode([ResultML].self, forKey: .results)
    }
}
/// Modelo para la información referente al paginado
struct Paging: Decodable {
    let total: Int
    let offset: Int
    let limit: Int

    enum CodingKeys: String, CodingKey {
        case total
        case offset
        case limit
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        total = try values.decode(Int.self, forKey: .total)
        offset = try values.decode(Int.self, forKey: .offset)
        limit = try values.decode(Int.self, forKey: .limit)
    }
}
/// Modelo principal para recabar toda la data de un item resultante de la búsqueda
struct ResultML: Decodable {
    let id: String
    let name: String
    var price: Float = 0.0
    var currencyId: String = ""
    var condition: String = ""
    var thumbnail: String = ""
    var acceptsMercadopago: Bool = false
    let shipping: Shipping

    enum CodingKeys: String, CodingKey {
        case id
        case name = "title"
        case price
        case concurrencyId = "currency_id"
        case condition
        case thumbnail
        case acceptsMercadopago = "accepts_mercadopago"
        case seller
        case shipping
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        price = try values.decode(Float.self, forKey: .price)
        currencyId = try values.decode(String.self, forKey: .concurrencyId)
        condition = try values.decode(String.self, forKey: .condition)
        thumbnail = try values.decode(String.self, forKey: .thumbnail)
        acceptsMercadopago = try values.decode(Bool.self, forKey: .acceptsMercadopago)
//        seller = try values.decode(Seller.self, forKey: .seller)
        shipping  = try values.decode(Shipping.self, forKey: .shipping)
    }

}

/// Modelo para identificar las características del envio a las que esta sujetas el item
struct Shipping: Decodable {
    let freeShipping: Bool
    let logisticType: String

    enum CodingKeys: String, CodingKey {
        case freeShipping = "free_shipping"
        case logisticType = "logistic_type"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        freeShipping = try values.decode(Bool.self, forKey: .freeShipping)
        logisticType = try values.decode(String.self, forKey: .logisticType)
    }
}
