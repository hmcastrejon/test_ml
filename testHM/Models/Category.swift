//
//  Category.swift
//  testHM
//
//  Created by Hugo Castrejon on 06/02/22.
//

import Foundation
/// Modelo principal para describir las categorías
struct Category: Decodable {
    let id: String
    let name: String

    enum CodingKeys: String, CodingKey {
    case id
    case name
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
    }
}
