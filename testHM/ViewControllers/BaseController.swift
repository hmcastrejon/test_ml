//
//  BaseController.swift
//  testHM
//
//  Created by Hugo Castrejon on 04/02/22.
//

import Foundation
import UIKit
import WebKit
class BaseController: UIViewController {
    lazy var refreshController: UIRefreshControl = {
        let controller = UIRefreshControl()
        controller.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        return controller
    }()
    var state: ActivityState = .loading
    override func viewDidLoad() {
        setup()
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @objc func refresh() {
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
    func setup() {}
    func setupNavigationBar() {
        self.navigationController?.isNavigationBarHidden = false
        navigationItem.hidesBackButton = true
        navigationItem.title = Constant.TitleViews.categoryViewController
        let backButton = UIBarButtonItem(image: UIImage(systemName: "chevron.backward"),
                                         style: .plain,
                                         target: self,
                                         action: #selector(tappedBackButton))

        navigationItem.leftBarButtonItem = backButton
    }
    @objc func tappedBackButton() {
        let back = navigationController?.popViewController(animated: true)
        if back == nil {
            dismiss(animated: true, completion: nil)
        }
    }
}
