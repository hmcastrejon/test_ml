//
//  ErrroCell.swift
//  testHM
//
//  Created by Hugo Castrejon on 07/02/22.
//

import UIKit

protocol ErrorCellDelegate: AnyObject {
    func didTappedRefreshButton()
}

class ErrorEmptyCell: UIView, ReusableView {
    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = .gray
        return label
    }()

    private let refreshButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .body)
        button.setImage(UIImage(systemName: "return"), for: .normal)
        button.setTitle(Constant.TitleViews.categoryActionableBtn, for: .normal)
        return button
    }()
    weak var delegate: ErrorCellDelegate?

    override init (frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        self.refreshButton.addTarget(self, action: #selector(refresAction), for: .touchUpInside)
        addSubviews(titleLabel, refreshButton)
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: self.topAnchor),
            titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
            titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
            refreshButton.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: -16),
            refreshButton.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            refreshButton.widthAnchor.constraint(equalToConstant: 20),
            refreshButton.heightAnchor.constraint(equalToConstant: 20),
            refreshButton.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }

    func configure(with type: ActivityState) {
        switch type {
        case .empty:
            titleLabel.text = Constant.ApiServiceMessages.emptyView
            refreshButton.isHidden = true
        case .error:
            titleLabel.text =  Constant.ApiServiceMessages.errorOnSearch
            refreshButton.isHidden = false
        case.loading:
            titleLabel.text = Constant.ApiServiceMessages.searchingitems
            refreshButton.isHidden = true
        default:
            titleLabel.text = ""
            refreshButton.isHidden = true
        }
    }

    @objc
    private func refresAction() {
        delegate?.didTappedRefreshButton()
    }
}
