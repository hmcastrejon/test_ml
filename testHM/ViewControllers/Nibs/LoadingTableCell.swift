//
//  LoadingTableCell.swift
//  testHM
//
//  Created by Hugo Castrejon on 07/02/22.
//

import Foundation
import UIKit
class LoadingTableCell: UITableViewHeaderFooterView {
    let activityIdicator: UIActivityIndicatorView = {
        let activityIdicator = UIActivityIndicatorView()
        activityIdicator.translatesAutoresizingMaskIntoConstraints = false
        activityIdicator.style = .medium
        return activityIdicator
    }()

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        addSubview(activityIdicator)
        activityIdicator.startAnimating()
        NSLayoutConstraint.activate([
            activityIdicator.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            activityIdicator.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            self.heightAnchor.constraint(equalToConstant: 50)
        ])
    }

}
