//
//  SearchItemCell.swift
//  testHM
//
//  Created by Hugo Castrejon on 07/02/22.
//
import UIKit
class ItemCell: UITableViewCell {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var shippingInfoLabel: UILabel!
    @IBOutlet weak var fullIco: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setupView(itemValue: ResultML) {
        self.backgroundColor = .white
        productImage.getImage(withUrl: itemValue.thumbnail)
        fullIco.isHidden = true
        priceLabel.text = itemValue.price.toCurrencyFormat()
        descriptionLabel.text = itemValue.name
        if let logisticType: LogisticType = LogisticType(rawValue: itemValue.shipping.logisticType) {
            if logisticType == .fulfillment {
                fullIco.isHidden = false
            }
            shippingInfoLabel.text = UtilsInteractor().logisticText(logistic: logisticType, isFree: itemValue.shipping.freeShipping)
        }
    }
}
