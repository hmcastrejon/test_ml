//
//  ItemCategoryCell.swift
//  testHM
//
//  Created by Hugo Castrejon on 06/02/22.
//

import Foundation
import UIKit
class ItemCategoryCell: UITableViewCell, ReusableView {
    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        addSubview(nameLabel)
        nameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        nameLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20).isActive = true
    }

    func configure(title: String, selected: Bool) {
        nameLabel.text = title
        if selected {
            nameLabel.textColor = .white
            self.backgroundColor = .systemGreen
        } else {
            nameLabel.textColor = .gray
            self.backgroundColor = .white
        }
    }
}
