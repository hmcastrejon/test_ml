//
//  CategoriesCell.swift
//  testHM
//
//  Created by Hugo Castrejon on 05/02/22.
//

import UIKit
final class LastViewedCell: UICollectionViewCell, ReusableView {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configure(title: String) {
        self.titleLabel.text = title
    }
}

private extension LastViewedCell {
    func setUp() {
        setupConstraintsAndStyle()
    }

    func setupConstraintsAndStyle() {
        self.addSubview(self.titleLabel)
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 1
        backgroundColor = Constant.Colors.mlYellow
        self.setRoundedCorners(with: 10)
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10),
            titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10)
        ])
    }
}
