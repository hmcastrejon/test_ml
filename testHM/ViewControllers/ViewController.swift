//
//  ViewController.swift
//  testHM
//
//  Created by Hugo Castrejon on 04/02/22.
//

import UIKit
import Foundation

class ViewController: BaseController {
    // MARK: - UI objects declaration
    private let searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.backgroundColor = .white
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.placeholder = Constant.PlaceHolders.searchBar
        searchBar.searchBarStyle = .minimal
        searchBar.searchTextField.textColor = .darkText
        searchBar.setRoundedCorners(with: 15.0, maskToBounds: true)
        return searchBar
    }()

    private let categoriesBtn: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .body)
        button.setImage(UIImage(systemName: "list.bullet.circle"), for: .normal)
        button.setTitle(Constant.TitleViews.categoryActionableBtn, for: .normal)
        return button
    }()

    lazy var tableView: PaginatedTableView = {
        let table = PaginatedTableView(frame: .zero, style: .grouped)
        table.register(UINib(nibName: "ItemCell", bundle: nil), forCellReuseIdentifier: "itemCellID")
        table.translatesAutoresizingMaskIntoConstraints = false
        table.tableFooterView = UIView(frame: CGRect.zero)
        table.backgroundColor = .cyan
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 250
//        table.separatorStyle = .singleLine
        table.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        table.addSubview(self.refreshController)
        return table
    }()

    // MARK: - global variables
    private var stackView: UIStackView! = nil
    private let freeview = UIView()
    //    private var scrollToTopButton: UIButton?
    //    private var lastViewedCollectionView: UICollectionView! = nil
    private let viewModel = HomeViewModel()
    private var lastquery: String = ""

    /// Construye y formatea la vista que se le asignara como titleview al navigation controller, La vista incluye el buscador y un boton
    override func setupNavigationBar() {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.setupStyle()
    }
    /// Ejecuta el setup de la clase, este metodo es heredado del basecontroller
    override func setup() {
        searchBar.delegate = self
        self.uiSetup()
        //        viewModel.loadLastViews()
        viewModelSetup()
    }
    // MARK: - View Setup
    /// Ejecuta el setup de la interface
    private func uiSetup() {
        searchBar.removeBackground()
        self.categoriesBtn.addTarget(self, action: #selector(showCategoriesController), for: .touchUpInside)
        configureStackView()
    }

    /// Construye y formatea el StackView que arma la estructura del home
    private func configureStackView() {
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.backgroundColor = .white
        stackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stackView)
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 0),
            stackView.leftAnchor.constraint(equalTo: safeArea.leftAnchor, constant: 0),
            stackView.widthAnchor.constraint(equalTo: safeArea.widthAnchor),
            stackView.heightAnchor.constraint(equalTo: safeArea.heightAnchor)
        ])
        searchContainer()
        setupCategoriesBtn()
        setupResultsTableView()
        freeview.backgroundColor = .white
        stackView.addArrangedSubview(freeview)
        NSLayoutConstraint.activate([
            freeview.leftAnchor.constraint(equalTo: stackView.leftAnchor),
            freeview.rightAnchor.constraint(equalTo: stackView.rightAnchor),
            freeview.centerXAnchor.constraint(equalTo: stackView.centerXAnchor),
            freeview.bottomAnchor.constraint(equalTo: stackView.bottomAnchor)
        ])
    }
    private func searchContainer() {
        let viewForSearcher = UIView()
        viewForSearcher.backgroundColor = Constant.Colors.mlYellow
        viewForSearcher.translatesAutoresizingMaskIntoConstraints = false
        viewForSearcher.addSubview(searchBar)
        let spacing: CGFloat = 16
        NSLayoutConstraint.activate([
            searchBar.leftAnchor.constraint(equalTo: viewForSearcher.leftAnchor, constant: spacing),
            searchBar.rightAnchor.constraint(equalTo: viewForSearcher.rightAnchor, constant: -spacing),
            searchBar.topAnchor.constraint(equalTo: viewForSearcher.topAnchor, constant: 10)
        ])
        stackView.addArrangedSubview(viewForSearcher)
    }

    private func setupCategoriesBtn() {
        stackView.addArrangedSubview(categoriesBtn)
        NSLayoutConstraint.activate([
            categoriesBtn.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 16),
            categoriesBtn.widthAnchor.constraint(equalTo: stackView.widthAnchor),
            categoriesBtn.heightAnchor.constraint(equalToConstant: 40)
        ])
    }

    private func setupResultsTableView() {
        view.addSubview(tableView)
        self.state = .empty
        tableView.paginatedDelegate = self
        tableView.paginatedDataSource = self
        self.tableView.contentInsetAdjustmentBehavior = .never
        NSLayoutConstraint.activate([
            tableView.leftAnchor.constraint(equalTo: stackView.leftAnchor, constant: 16),
            tableView.rightAnchor.constraint(equalTo: stackView.rightAnchor, constant: -16),
            tableView.bottomAnchor.constraint(equalTo: stackView.bottomAnchor)
        ])
        tableView.topAnchor.constraint(equalTo: categoriesBtn.bottomAnchor).isActive = true
        tableView.register(UINib(nibName: "SearchItem", bundle: nil), forCellReuseIdentifier: "SearchItemID")
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: Notification.Name(rawValue: "END_RESERVATION"), object: nil)
        tableView.backgroundColor = .white
        tableView.estimatedRowHeight = 390
        tableView.keyboardDismissMode = .onDrag
        tableView.rowHeight = UITableView.automaticDimension
    }

    @objc func showCategoriesController() {
        let controller = CategoriesController(nibName: nil, bundle: nil)
        controller.selectedIndex = viewModel.selectedCategoryIndex
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    /// Este setup permite recibir los clousures del viewmodel para reaccionar en la vista
    private func viewModelSetup() {
        viewModel.updateLoadingStatus = {[weak self] (state) in
            DispatchQueue.main.async {
                self?.state = state
                switch state {
                case .success:
                    self?.view.endEditing(true)
                    self?.tableView.separatorStyle = .singleLine
                    self?.tableView.scrollsToTop = true
                    self?.searchBar.isUserInteractionEnabled = true
                case .loading:
                    self?.searchBar.isUserInteractionEnabled = false
                default:
                    self?.searchBar.isUserInteractionEnabled = true
                }
                self?.tableView.reloadData()
            }
        }
    }
}

// MARK: - didTappedRefreshButton
extension ViewController: ErrorCellDelegate {
    func didTappedRefreshButton() {
        self.view.endEditing(true)
        self.tableView.scrollsToTop = true
        self.tableView.reloadData()
    }
}

// MARK: - delegados del UISearchBar
extension ViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let query = searchBar.text, state != .loading {
            self.lastquery = query
            self.viewModel.loadItems(querty: query.queryfy(), fromSearchBar: true)
        }
    }
}
// MARK: - Delegados de CategoriesControllerDelegate
extension ViewController: CategoriesControllerDelegate {
    func didTapCategoryCell(item: Category, selectedCategoryIndex: Int) {
        viewModel.categorySelected = item
        viewModel.selectedCategoryIndex = selectedCategoryIndex
        if searchBar.text?.count ?? 0 > 0 {
            searchBarSearchButtonClicked(searchBar)
        }
        categoriesBtn.setTitle(Constant.TitleViews.filterCategoryBtn + (viewModel.categorySelected?.name ?? ""), for: .normal)
    }
}
// MARK: - Delegados de PaginatedTableViewDelegate
extension ViewController: PaginatedTableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }

    func loadMore(_ pageNumber: Int, _ pageSize: Int, onSuccess: ((Bool) -> Void)?, onError: ((Error) -> Void)?) {
        if pageNumber > 0 {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            tableView.layoutIfNeeded()
        }
        if pageNumber < self.viewModel.maxPages {
            self.viewModel.currentPage = pageNumber
            self.viewModel.loadItems(querty: self.lastquery.queryfy())
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                onSuccess?(true)
            }
        } else {
            self.state = .error
            let error: ApiServicesError = .unkownError(Constant.ApiServiceMessages.lastPage)
            onError?(error)
        }
    }
}

// MARK: DataSource de PaginatedTableViewDataSource
extension ViewController: PaginatedTableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = viewModel.getItemsNumberForSearch
        if count == 0 { self.state = .empty}
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "itemCellID") as? ItemCell {
            if let item = viewModel.getSearchItem(index: indexPath.row) { cell.setupView(itemValue: item)}
            return cell
        } else { return UITableViewCell()}
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = DetailViewController(nibName: "detailController", bundle: nil)
        controller.viewModel.result = viewModel.getSearchItem(index: indexPath.row)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if state == .error || state == .empty || state == .loading {
            return 120
        } else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let myViews = tableView.subviews.compactMap({$0 as? ErrorEmptyCell})
        if myViews.count == 0 {
            self.tableView.separatorStyle = .none
            let view = UIView(frame: CGRect(x: 0, y: 0, width: Constant.main.width, height: 150))
            let optionsView = ErrorEmptyCell()
            optionsView.configure(with: state)
            view.addSubview(optionsView)
            return optionsView
        } else {
            return UIView()
        }
    }
}
