//
//  CategoriesViewController.swift
//  testHM
//
//  Created by Hugo Castrejon on 06/02/22.
//

import Foundation
import UIKit

protocol CategoriesControllerDelegate: AnyObject {
    func didTapCategoryCell(item: Category, selectedCategoryIndex: Int)
}

class CategoriesController: BaseController {
    lazy var tableView: UITableView = {
        let table = UITableView()
        table.delegate = self
        table.dataSource = self
        table.register(ItemCategoryCell.self)
        table.tableFooterView = UIView(frame: CGRect.zero)
        table.indicatorStyle = .black
        table.separatorInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        return table
    }()

    weak var delegate: CategoriesControllerDelegate?
    private let viewModel = CateriesViewModel()
    var selectedIndex = -1

    override func setup() {
        self.navigationController?.navigationItem.title = Constant.TitleViews.categoryViewController
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tableView)
        tableView.backgroundColor = .white
        NSLayoutConstraint.activate([
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        viewModel.load()
    }

    /// Este setup permite recibir los clousures del viewmodel para reaccionar en la vista
    private func viewModelSetup() {
        viewModel.updateLoadingStatus = {[weak self] (state) in
            DispatchQueue.main.async {
                switch state {
                case .success:
                    self?.tableView.reloadData()
                default:
                    break
                }
            }
        }
    }
}

extension CategoriesController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.categoryCount > 0 { print(viewModel.categoryCount)}
        return viewModel.categoryCount

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as ItemCategoryCell
        let item = viewModel.getCategory(index: indexPath.row)
        cell.configure(title: item.name, selected: indexPath.row == selectedIndex ? true : false)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            delegate?.didTapCategoryCell(item: viewModel.getCategory(index: indexPath.row), selectedCategoryIndex: indexPath.row)
            self.tappedBackButton()
    }
}
