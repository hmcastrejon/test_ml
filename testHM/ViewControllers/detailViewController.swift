//
//  detailViewController.swift
//  testHM
//
//  Created by Hugo Castrejon on 09/02/22.
//

import Foundation
import UIKit
class DetailViewController: BaseController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var productStateLbl: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var mpIconImageView: UIImageView!
    @IBOutlet weak var shippingLbl: UILabel!
    @IBOutlet weak var fullLabelLbl: UIImageView!
    let viewModel = DetailViewModel()

    /// setup the view with the model data
    override func setup() {
        titleLbl.text = viewModel.result?.name
        productStateLbl.text = viewModel.result?.condition
        if let urlStr = viewModel.result?.thumbnail {
            thumbnailImageView.getImage(withUrl: urlStr)
        }
        priceLbl.text = viewModel.result?.price.toCurrencyFormat()
        mpIconImageView.isHidden = viewModel.result?.acceptsMercadopago ?? false ? false : true
        if let resultUnwrapped = viewModel.result, let logisticType: LogisticType = LogisticType(rawValue: resultUnwrapped.shipping.logisticType) {
            if logisticType == .fulfillment {
                fullLabelLbl.isHidden = false
            } else { fullLabelLbl.isHidden = true}
            shippingLbl.text = UtilsInteractor().logisticText(logistic: logisticType, isFree: resultUnwrapped.shipping.freeShipping)
        }
        titleLbl.text = viewModel.result?.name

    }
}
