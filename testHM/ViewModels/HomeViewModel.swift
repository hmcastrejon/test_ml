//
//  HomeViewModel.swift
//  testHM
//
//  Created by Hugo Castrejon on 06/02/22.
//

import Foundation
class HomeViewModel {
    var categorySelected: Category?
    var selectedCategoryIndex = -1
    var currentPage: Int = 0
    private var search: Search?
//    private var lastViewsArray: [ResultML]?
    var updateLoadingStatus: ((_ isloading: ActivityState) -> Void)?
    var state: ActivityState = .empty {
        didSet {
            self.updateLoadingStatus?(state)
        }
    }
//    var reloadLastViews: (() -> Void)?

    /// Variable que proporciona el total de ítems de la busqueda de una página (ver lógica de paginado)
    var getItemsNumberForSearch: Int {
        return search?.results.count ?? 0
    }

    /// Variable para el control del máximo de páginas
    var maxPages: Int {
        if let values = search {
            let countPages = round(Float(values.paging.total) / Float(Constant.main.itemsPerPage))
            return Int(countPages)
        } else {
            return 0
        }
    }

    /// Arma la consulta y manda pedir la busqueda de items,  esta función tambien contiene la lógica para buscar en una sola categoría y paginado
    /// - Parameter queryStr: Cadena con al url + el texto del searchbar
    /// - fromSearchBar queryStr: Bandera que nos permite resetear currentpage
    func loadItems(querty queryStr: String, fromSearchBar: Bool = false) {
        var filteredQuerty = ""
        if fromSearchBar { currentPage = 0}
        if categorySelected != nil, let cat = categorySelected?.id {
            filteredQuerty = queryStr + Constant.UrlServices.searchWithCategoryFilter + cat
            filteredQuerty += Constant.UrlServices.searchWithLimit + String("\(Constant.main.itemsPerPage)")
            filteredQuerty += Constant.UrlServices.searchWithPageNumber + String("\(currentPage * Constant.main.itemsPerPage)")
        } else {
            filteredQuerty = queryStr + Constant.UrlServices.searchWithLimit + String("\(Constant.main.itemsPerPage)")
            filteredQuerty += Constant.UrlServices.searchWithPageNumber + String("\(currentPage * Constant.main.itemsPerPage)")
        }
        print(filteredQuerty)
        self.state = .loading
        ApiServiceInteractor().performRequest(type: Search.self, requestURLStr: filteredQuerty) { [weak self] result in
            switch result {
            case .success(let objResponse):
                if self?.currentPage == 0 {
                    self?.search = objResponse
                } else {
                        self?.search?.paging = objResponse.paging
                        self?.search?.results.append(contentsOf: objResponse.results)
                    }
                print("Resultados de la busqueda 🔥: \(objResponse.results.count), currentPage: \(String(describing: self?.currentPage))")
                self?.state = .success
            case .failure(let requestError):
                print(requestError)
                self?.state = .error
            }
        }
    }

    /// Regresa un item de la busqueda en especifico, ideal para rellenar el contenido de las celdas de la tabla paginada
    /// - Parameter index: Puntero que especifica que item del vector se esta solicitando
    /// - Returns: regresa 1 resultado en un objeto ResultML, este contiene todas las cáracterísticas del item
    func getSearchItem(index: Int) -> ResultML? {
        if let item = search?.results[index] {
            return item
        }
        return nil
    }
}
