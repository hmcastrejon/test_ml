//
//  CategoriesViewModel.swift
//  testHM
//
//  Created by Hugo Castrejon on 07/02/22.
//

import Foundation
class CateriesViewModel {
    private var categories: [Category] = []
    var updateLoadingStatus: ((_ isloading: ActivityState) -> Void)?
    var state: ActivityState = .loading {
        didSet {
            self.updateLoadingStatus?(state)
        }
    }

    /// Variable que proporciona el total de categorías
    var categoryCount: Int {
        return categories.count
    }

    /// Funcion publica para solicitar las características en el arranque de la app
    func load() {
        callCategories()
    }

    /// Arma la consulta y manda pedir las categorías disponibles
    private func callCategories() {
        state = .loading
        ApiServiceInteractor().performRequest(type: [Category].self, requestURLStr: Constant.UrlServices.categoryList) { [weak self] result in
            switch result {
            case .success(let objResponse):
                self?.categories = objResponse
                self?.state = .success
            case .failure(let requestError):
                print(requestError)
                self?.state = .error
            }
        }
    }

    /// Regresa un item del vector de caraterísticas
    /// - Parameter index: Puntero que especifica que item del vector se esta solicitando
    /// - Returns: regresa 1 resultado en un objeto Category, este contiene todas las cáracterísticas de la categoría
    func getCategory(index: Int) -> Category {
        return categories[index]
    }
}
