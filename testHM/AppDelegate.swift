//
//  AppDelegate.swift
//  testHM
//
//  Created by Hugo Castrejon on 04/02/22.
//

import UIKit
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let initialVC = ViewController()
        let navigationController = UINavigationController(rootViewController: initialVC)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()

        return true    }

}
