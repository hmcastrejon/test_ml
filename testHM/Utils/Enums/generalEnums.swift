//
//  generalEnums.swift
//  testHM
//
//  Created by Hugo Castrejon on 06/02/22.
//

import UIKit

/// Enum que describe los estados de interacción entre el viewModel y las vistas
enum ActivityState: String {
    case loading
    case error
    case success
    case validation
    case empty
}

/// Enum que describe los resultados conocidos para envios de los items de búsqueda
enum LogisticType: String {
    case fulfillment // Full
    case crossDocking = "cross_docking" // Envío gratis a todo el país
    case dropOff = "drop_off" // Envío gratis a todo el país
    case xdDropOff = "xd_drop_off" // Llega gratis mañana
    case unknown
}

enum ConditionType: String {
    case new
    case used
}

/// Enum para gestionar los errores posibles en el ApiServicesInteractor
enum ApiServicesError: Error, LocalizedError {
    case networkError
    case invalidResponse(AnyObject?)
    case unkownError(String)
    case serverError(Error)
}

public enum SheetSize {
    case fixed(CGFloat)
    case halfScreen
    case fullScreen
}
