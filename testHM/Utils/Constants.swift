//
//  Constants.swift
//  testHM
//
//  Created by Hugo Castrejon on 04/02/22.
//

import UIKit
// swiftlint:disable type_name
/// Estructura de constantes que bajo anidación permite organizar y simplificar los textos estaticos de la aplicación. Esta solución en una buena práctica y base para escalar a una aplicación con múltiples traducciones.
struct Constant {
    struct main {
        static let width: CGFloat = UIScreen.main.bounds.size.width
        static let height: CGFloat = UIScreen.main.bounds.size.height
        static let itemsPerPage = 20
    }
    struct DateTime {
        static let iso8601Full = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        static let fomatterId = "es_AR"
    }

    class Colors {
        static let mlYellow: UIColor = UIColor(named: "yellow_ml") ?? UIColor.yellow
        static let mlGreen: UIColor = UIColor(named: "green_ml") ?? UIColor.green
    }
    // MARK: - Text for Messages and Console
    struct ApiServiceMessages {
        static let unwrappingData = "Error on data unwrapping"
        static let unwrappingURL = "Error getting the URL"
        static let lastPage = "This is the last page"
        static let emptyView = "Hey!, busquemos algún articulo. Para obtener mejores resultados, puedes ayudarte con busquedas en 1 sola categoria"
        static let errorOnSearch = "Ocurrio un problema al cargar el contenido, puedes reintentarlo dando click en la flecha"
        static let searchingitems = "Buscando productos"

    }

    // MARK: - Text for UI
    struct TitleViews {
        static let categoryViewController = "Categorías"
        static let categoryActionableBtn = "  Filtrar por categorías"
        static let lastViewed = "Últimos visitados"
        static let filterCategoryBtn = "  Buscando en "
    }
    struct PlaceHolders {
        static let searchBar = "Buscar productos"
    }
    struct UrlServices {
        static let categoryList = "https://api.mercadolibre.com/sites/MLA/categories"
        static let searchBasePath = "https://api.mercadolibre.com/sites/MLA/search?q="
        static let searchWithCategoryFilter = "&category="
        static let searchWithPageNumber = "&offset="
        static let searchWithLimit = "&limit="
    }
}
