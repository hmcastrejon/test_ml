////
////  StorageInteractor.swift
////  testHM
////
////  Created by Hugo Castrejon on 08/02/22.
////
//
// import Foundation
// class SetUserDefaultsValueInteractor {
//    /// Intercator for save a Any type value
//    /// - Parameters:
//    ///   - object: Variable, could be any type
//    ///   - key: Constant Text
//    ///   - Call Example: SetUserDefaultsValueInteractor().execute(object: Variable, key: ConstantText)
//    func execute(object: Any, key: String) {
//        let defaults: UserDefaults = UserDefaults.standard
//        defaults.set(object, forKey: key)
//        defaults.synchronize()
//    }
//    /// Intercator for save a Generic Codable object
//    /// - Parameters:
//    ///   - model: Codable Model type
//    ///   - key: Constant Text
//    ///   - Call Example: SetUserDefaultsValueInteractor().execute(model: Model, key: ConstantText)
//    func saveGenericData<T: Codable>(model: T, key: String) {
//        let encoder = JSONEncoder()
//        if let encoded = try? encoder.encode(model) {
//            let defaults = UserDefaults.standard
//            defaults.set(encoded, forKey: key)
//        }
//    }
//
// }
///// Intercator for Get a Any Generic Codable object. Call Example: GetUserDefaultsValueInteractor<SomeDecodableModel or Kind Class>().execute(key: ConstantText)
// class GetUserDefaultsValueInteractor<T> {
//    func execute(key: String) -> T? {
//        let defaults: UserDefaults = UserDefaults.standard
//        return defaults.object(forKey: key) as? T
//    }
// }
