//
//  ApiService.swift
//  testHM
//
//  Created by Hugo Castrejon on 06/02/22.
//

import Foundation
class ApiServiceInteractor: NSObject {
    /// Maneja las peticiones a travéz de datatask y transforma la respuesta en un modelo que erteneza al protocolo Decodable seteado desde un parametro Generic, La respuesta es devuelta en en un clousure para fácilitar su procesamiento e hilado de tareas.
    func performRequest<T: Decodable>(type: T.Type, requestURLStr: String, _ completion: @escaping (Result <T, Error>) -> Void) {
        if let url = URL(string: requestURLStr) {
            let session = URLSession.shared
            session.dataTask(with: url, completionHandler: {data, _, error -> Void in
                if let data = data {
                    if let items = try? JSONDecoder().decode(type.self, from: data) {
                        completion(.success(items))
                    } else {
                        completion(.failure(ApiServicesError.unkownError(Constant.ApiServiceMessages.unwrappingData)))
                    }
                }
                if error != nil {
                    completion(.failure(ApiServicesError.networkError))
                }
            }).resume()
        } else {
            let customError = ApiServicesError.unkownError(Constant.ApiServiceMessages.unwrappingURL)
            completion(.failure(customError))
        }
    }
}
