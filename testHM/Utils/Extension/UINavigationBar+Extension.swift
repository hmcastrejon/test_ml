//
//  UINavigationBar+Extension.swift
//  testHM
//
//  Created by Hugo Castrejon on 05/02/22.
//

import Foundation
import UIKit

extension UINavigationBar {
    func setupStyle() {
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = Constant.Colors.mlYellow
        appearance.titleTextAttributes = [.foregroundColor: UIColor.black]
        appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.black]
        appearance.shadowColor = .clear
        self.tintColor = .black
        self.standardAppearance = appearance
        self.compactAppearance = appearance
        self.scrollEdgeAppearance = appearance
        self.layoutIfNeeded()
    }
}
