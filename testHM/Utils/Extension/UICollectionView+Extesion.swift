//
//  UICollectionView+Extesion.swift
//  testHM
//
//  Created by Hugo Castrejon on 05/02/22.
//

import Foundation

import UIKit

extension UICollectionView {
    // MARK: - UICollectionViewCell
    /// Registers a cell that conforms the ReusableView and NibLoadableView protocols
    /// - Parameter _: Class of the cell to be registered
    func register<T: UICollectionViewCell>(_: T.Type) where T: ReusableView, T: NibLoadableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibName, bundle: bundle)
        register(nib, forCellWithReuseIdentifier: T.reuseIdentifier)
    }

    /// Register a cell that conforms the ReusableView protocol
    /// - Parameter _: Class of  the cell to be registered
    func register<T: UICollectionViewCell>(_: T.Type) where T: ReusableView {
        register(T.self, forCellWithReuseIdentifier: T.reuseIdentifier)
    }

    /// Register a reusable view that conforms to ReusableView
    /// - Parameter _: Class of the View to be registered
    func register<T: UICollectionReusableView>(_: T.Type, for supplementaryViewOfKind: String) where T: ReusableView {
        register(T.self, forSupplementaryViewOfKind: supplementaryViewOfKind, withReuseIdentifier: T.reuseIdentifier)
    }

    /// Register a reusable view that conforms to ReusableView and NibLoadableView protocols
    /// - Parameter _: Class of the View to be registered
    func register<T: UICollectionReusableView>(_: T.Type, for supplementaryViewOfKind: String) where T: ReusableView, T: NibLoadableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibName, bundle: bundle)
        register(nib, forSupplementaryViewOfKind: supplementaryViewOfKind, withReuseIdentifier: T.reuseIdentifier)
    }

    /// Dequeue a cell that conforms the ReusableView protocol
    /// - Parameter indexPath: indexPath of the cell
    func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T where T: ReusableView {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        return cell
    }
}
