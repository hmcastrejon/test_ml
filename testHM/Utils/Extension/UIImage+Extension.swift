//
//  UIImageView+Extension.swift
//  testHM
//
//  Created by Hugo Castrejon on 05/02/22.
//

import UIKit
let imageCache = NSCache<NSString, UIImage>()
extension UIImageView {
    func getImage(withUrl urlString: String) {
        if let url = URL(string: urlString) {
            self.image = nil
            if let cachedImage = imageCache.object(forKey: urlString as NSString) {
                self.image = cachedImage
                return
            }
            let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.medium)
            addSubview(activityIndicator)
            activityIndicator.startAnimating()
            activityIndicator.center = self.center
            URLSession.shared.dataTask(with: url, completionHandler: { (data, _, error) in
                if error != nil {
                    print(error!)
                    return
                }

                DispatchQueue.main.async {
                    if let image = UIImage(data: data!) {
                        imageCache.setObject(image, forKey: urlString as NSString)
                        self.image = image
                        activityIndicator.removeFromSuperview()
                    }
                }
            }).resume()
        }
    }
}
