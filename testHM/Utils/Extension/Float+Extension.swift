//
//  Float+Extension.swift
//  testHM
//
//  Created by Hugo Castrejon on 09/02/22.
//

import Foundation

extension Float {
    func toCurrencyFormat() -> String {
        let formatter = NumberFormatter()
        formatter.locale =  Locale(identifier: Constant.DateTime.fomatterId)
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: self as NSNumber) {
            return formattedTipAmount
        }
        return ""
    }
}
