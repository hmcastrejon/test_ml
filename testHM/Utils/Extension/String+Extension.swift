//
//  String+Extension.swift
//  testHM
//
//  Created by Hugo Castrejon on 06/02/22.
//

import Foundation
extension String {
    func queryfy() -> String {
        if let escapedString = self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
            return Constant.UrlServices.searchBasePath + escapedString
        } else {
            return ""
        }
    }
    func urlEncondingString() -> String {
        if let escapedString = self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
            return escapedString
        } else {
            return ""
        }
    }

}
