//
//  UIView+Extension.swift
//  testHM
//
//  Created by Hugo Castrejon on 04/02/22.
//

import Foundation
import UIKit

public extension UIView {
    func setRoundedCorners(with radius: CGFloat, maskToBounds: Bool = false) {
        layer.masksToBounds = maskToBounds
        layer.cornerRadius = radius
        layer.borderWidth = 1
        layer.borderColor = UIColor.lightGray.cgColor
    }
    func elevate(elevation: Double) {
       self.layer.masksToBounds = false
       self.layer.shadowColor = UIColor.black.cgColor
       self.layer.shadowOffset = CGSize(width: 0, height: elevation)
       self.layer.shadowRadius = abs(CGFloat(elevation))
       self.layer.shadowOpacity = 0.2
    }
    func addSubviews(_ views: UIView...) {
        views.forEach({ self.addSubview($0) })
    }
    func allSubViewsOf<T: UIView>(type: T.Type) -> [T] {
           var all = [T]()
           func getSubview(view: UIView) {
               if let wrapped = view as? T, wrapped != self {
                   all.append(wrapped)
               }
               guard view.subviews.count>0 else { return }
               view.subviews.forEach { getSubview(view: $0) }
           }
           getSubview(view: self)
           return all
       }

}
