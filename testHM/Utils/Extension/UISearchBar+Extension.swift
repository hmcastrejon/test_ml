//
//  UISearchBar+Extension.swift
//  testHM
//
//  Created by Hugo Castrejon on 09/02/22.
//

import Foundation
import UIKit

public extension UISearchBar {
   func removeBackground() {
    guard let backgroundType = NSClassFromString("_UISearchBarSearchFieldBackgroundView") else { return }
    for view in self.allSubViewsOf(type: UIView.self) where view.isKind(of: backgroundType) {
      view.removeFromSuperview()
    }
  }
}
