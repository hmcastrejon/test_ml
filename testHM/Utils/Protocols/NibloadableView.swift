//
//  NibloadableView.swift
//  testHM
//
//  Created by Hugo Castrejon on 05/02/22.
//

import Foundation
import UIKit

protocol NibLoadableView: AnyObject {
    /// Represents the class name of a xib file
    static var nibName: String { get }
}

extension NibLoadableView where Self: UIView {
    /// Set the nib name to be equal to the class name
    static var nibName: String {
        return String(describing: self)
    }
}

extension NibLoadableView where Self: UIViewController {
    /// Set the nib name to be equal to the class name
    static var nibName: String {
        return String(describing: self)
    }
}
