//
//  ReusableView.swift
//  testHM
//
//  Created by Hugo Castrejon on 05/02/22.
//

import Foundation
import UIKit

protocol ReusableView: AnyObject {
    /// Represents the reusesable identifier for a cell
    static var reuseIdentifier: String { get }
}

extension ReusableView where Self: UIView {
    /// Set the reuse identifier to be equal to the class name
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
