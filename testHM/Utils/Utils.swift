//
//  Utils.swift
//  testHM
//
//  Created by Hugo Castrejon on 09/02/22.
//

import Foundation
class UtilsInteractor {
    func logisticText(logistic: LogisticType, isFree: Bool) -> String {
        var complementText = " "
        if isFree { complementText = " gratis "}
        switch logistic {
        case .fulfillment:
            return "Envío"+complementText+"a todo el país"
        case .crossDocking: return "Envío"+complementText+"a todo el país"
        case .dropOff: return "Envío"+complementText+"a todo el país"
        case .xdDropOff: return "Llega"+complementText+"mañana"
        case .unknown: return ""
        }
    }
}
