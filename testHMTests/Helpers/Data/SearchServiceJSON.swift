//
//  SearchServiceJSON.swift
//  testHMTests
//
//  Created by Hugo Castrejon on 14/02/22.
//

import Foundation

struct SearchServiceJSON: Mockeable {
    var jsonValue: String = """
    {
        "site_id": "MLA",
        "country_default_time_zone": "GMT-03:00",
        "query": "libro",
        "paging": {
            "total": 250172,
            "primary_results": 1000,
            "offset": 0,
            "limit": 3
        },
        "results": [
            {
                "id": "MLA917463855",
                "site_id": "MLA",
                "title": "Libros Gredos Usados",
                "seller": {
                    "id": 190724063,
                    "permalink": "http://perfil.mercadolibre.com.ar/DISTRIBUIDORADELIBROS",
                    "registration_date": "2015-08-21T20:19:31.000-04:00",
                    "car_dealer": false,
                    "real_estate_agency": false,
                    "tags": [
                        "normal",
                        "eshop",
                        "mshops",
                        "credits_profile",
                        "credits_priority_2",
                        "messages_as_seller"
                    ],
                    "eshop": {
                        "seller": 190724063,
                        "eshop_rubro": null,
                        "eshop_id": 429539,
                        "nick_name": "DISTRIBUIDORADELIBROS",
                        "site_id": "MLA",
                        "eshop_logo_url": "http://resources.mlstatic.com/eshops/190724063ved2b16.png",
                        "eshop_status_id": 1,
                        "eshop_experience": 0,
                        "eshop_locations": []
                    },
                    "seller_reputation": {
                        "power_seller_status": "platinum",
                        "level_id": "5_green",
                        "metrics": {
                            "cancellations": {
                                "period": "60 days",
                                "rate": 0,
                                "value": 1
                            },
                            "claims": {
                                "period": "60 days",
                                "rate": 0.002,
                                "value": 3
                            },
                            "delayed_handling_time": {
                                "period": "60 days",
                                "rate": 0.0248,
                                "value": 29
                            },
                            "sales": {
                                "period": "60 days",
                                "completed": 1423
                            }
                        },
                        "transactions": {
                            "canceled": 211,
                            "period": "historic",
                            "total": 7678,
                            "ratings": {
                                "negative": 0,
                                "neutral": 0.01,
                                "positive": 0.99
                            },
                            "completed": 7467
                        }
                    }
                },
                "price": 660,
                "prices": {
                    "id": "MLA917463855",
                    "prices": [
                        {
                            "id": "4",
                            "type": "standard",
                            "amount": 660,
                            "regular_amount": null,
                            "currency_id": "ARS",
                            "last_updated": "2022-01-19T16:44:29Z",
                            "conditions": {
                                "context_restrictions": [],
                                "start_time": null,
                                "end_time": null,
                                "eligible": true
                            },
                            "exchange_rate_context": "DEFAULT",
                            "metadata": {}
                        }
                    ],
                    "presentation": {
                        "display_currency": "ARS"
                    },
                    "payment_method_prices": [],
                    "reference_prices": [
                        {
                            "id": "11",
                            "type": "min_standard",
                            "conditions": {
                                "context_restrictions": [
                                    "channel_marketplace"
                                ],
                                "start_time": null,
                                "end_time": null,
                                "eligible": true
                            },
                            "amount": 600,
                            "currency_id": "ARS",
                            "exchange_rate_context": "DEFAULT",
                            "tags": [],
                            "last_updated": "2022-02-13T04:32:28Z"
                        },
                        {
                            "id": "12",
                            "type": "was",
                            "conditions": {
                                "context_restrictions": [
                                    "channel_marketplace"
                                ],
                                "start_time": "2022-02-13T04:32:28Z",
                                "end_time": "2022-02-20T04:32:28Z",
                                "eligible": true
                            },
                            "amount": 600,
                            "currency_id": "ARS",
                            "exchange_rate_context": "DEFAULT",
                            "tags": [],
                            "last_updated": "2022-02-13T04:32:28Z"
                        }
                    ],
                    "purchase_discounts": []
                },
                "sale_price": null,
                "currency_id": "ARS",
                "available_quantity": 1,
                "sold_quantity": 50,
                "buying_mode": "buy_it_now",
                "listing_type_id": "gold_special",
                "stop_time": "2041-04-17T04:00:00.000Z",
                "condition": "used",
                "permalink": "https://articulo.mercadolibre.com.ar/MLA-917463855-libros-gredos-usados-_JM",
                "thumbnail": "http://http2.mlstatic.com/D_738343-MLA48937115661_012022-I.jpg",
                "thumbnail_id": "738343-MLA48937115661_012022",
                "accepts_mercadopago": true,
                "installments": {
                    "quantity": 12,
                    "amount": 93.59,
                    "rate": 70.17,
                    "currency_id": "ARS"
                },
                "address": {
                    "state_id": "AR-C",
                    "state_name": "Capital Federal",
                    "city_id": "TUxBQkJPRTQ0OTRa",
                    "city_name": "Boedo"
                },
                "shipping": {
                    "free_shipping": false,
                    "mode": "me2",
                    "tags": [],
                    "logistic_type": "xd_drop_off",
                    "store_pick_up": false
                },
                "seller_address": {
                    "id": "",
                    "comment": "",
                    "address_line": "",
                    "zip_code": "",
                    "country": {
                        "id": "AR",
                        "name": "Argentina"
                    },
                    "state": {
                        "id": "AR-C",
                        "name": "Capital Federal"
                    },
                    "city": {
                        "id": "TUxBQkJPRTQ0OTRa",
                        "name": "Boedo"
                    },
                    "latitude": "",
                    "longitude": ""
                },
                "attributes": [
                    {
                        "source": 7092,
                        "id": "ITEM_CONDITION",
                        "value_id": "2230581",
                        "value_struct": null,
                        "values": [
                            {
                                "id": "2230581",
                                "name": "Usado",
                                "struct": null,
                                "source": 7092
                            }
                        ],
                        "attribute_group_name": "Otros",
                        "name": "Condición del ítem",
                        "value_name": "Usado",
                        "attribute_group_id": "OTHERS"
                    },
                    {
                        "attribute_group_name": "Otros",
                        "id": "WEIGHT",
                        "value_name": null,
                        "value_struct": null,
                        "attribute_group_id": "OTHERS",
                        "source": 3376461333454861,
                        "name": "Peso",
                        "value_id": "-1",
                        "values": [
                            {
                                "source": 3376461333454861,
                                "id": "-1",
                                "name": null,
                                "struct": null
                            }
                        ]
                    }
                ],
                "original_price": null,
                "category_id": "MLA412445",
                "official_store_id": null,
                "domain_id": "MLA-BOOKS",
                "catalog_product_id": null,
                "tags": [
                    "good_quality_picture",
                    "immediate_payment",
                    "cart_eligible",
                    "shipping_guaranteed"
                ],
                "order_backend": 1,
                "use_thumbnail_id": true,
                "offer_score": null,
                "offer_share": null,
                "match_score": null,
                "winner_item_id": null,
                "melicoin": null
            },
            {
                "id": "MLA862640188",
                "site_id": "MLA",
                "title": "Guia Oceano Brasil Libro",
                "seller": {
                    "id": 26309411,
                    "permalink": "http://perfil.mercadolibre.com.ar/CINENAUTA2010",
                    "registration_date": "2010-04-07T16:57:13.000-04:00",
                    "car_dealer": false,
                    "real_estate_agency": false,
                    "tags": [
                        "normal",
                        "credits_priority_1",
                        "credits_profile",
                        "mshops",
                        "messages_as_seller"
                    ],
                    "seller_reputation": {
                        "power_seller_status": "platinum",
                        "level_id": "5_green",
                        "metrics": {
                            "cancellations": {
                                "period": "60 days",
                                "rate": 0.0063,
                                "value": 8
                            },
                            "claims": {
                                "period": "60 days",
                                "rate": 0.0087,
                                "value": 11
                            },
                            "delayed_handling_time": {
                                "period": "60 days",
                                "rate": 0.0663,
                                "value": 69
                            },
                            "sales": {
                                "period": "60 days",
                                "completed": 1204
                            }
                        },
                        "transactions": {
                            "canceled": 405,
                            "period": "historic",
                            "total": 6906,
                            "ratings": {
                                "negative": 0.01,
                                "neutral": 0.01,
                                "positive": 0.98
                            },
                            "completed": 6501
                        }
                    }
                },
                "price": 210.78,
                "prices": {
                    "id": "MLA862640188",
                    "prices": [
                        {
                            "id": "20",
                            "type": "standard",
                            "amount": 210.78,
                            "regular_amount": null,
                            "currency_id": "ARS",
                            "last_updated": "2022-01-26T21:26:49Z",
                            "conditions": {
                                "context_restrictions": [],
                                "start_time": null,
                                "end_time": null,
                                "eligible": true
                            },
                            "exchange_rate_context": "DEFAULT",
                            "metadata": {}
                        }
                    ],
                    "presentation": {
                        "display_currency": "ARS"
                    },
                    "payment_method_prices": [],
                    "reference_prices": [],
                    "purchase_discounts": []
                },
                "sale_price": null,
                "currency_id": "ARS",
                "available_quantity": 1,
                "sold_quantity": 1,
                "buying_mode": "buy_it_now",
                "listing_type_id": "gold_special",
                "stop_time": "2040-06-11T04:00:00.000Z",
                "condition": "used",
                "permalink": "https://articulo.mercadolibre.com.ar/MLA-862640188-guia-oceano-brasil-libro-_JM",
                "thumbnail": "http://http2.mlstatic.com/D_890604-MLA42231134720_062020-I.jpg",
                "thumbnail_id": "890604-MLA42231134720_062020",
                "accepts_mercadopago": true,
                "installments": {
                    "quantity": 12,
                    "amount": 29.89,
                    "rate": 70.17,
                    "currency_id": "ARS"
                },
                "address": {
                    "state_id": "AR-C",
                    "state_name": "Capital Federal",
                    "city_id": "TUxBQkNPTDI3NDNa",
                    "city_name": "Colegiales"
                },
                "shipping": {
                    "free_shipping": false,
                    "mode": "me2",
                    "tags": [
                        "self_service_in"
                    ],
                    "logistic_type": "xd_drop_off",
                    "store_pick_up": false
                },
                "seller_address": {
                    "id": "",
                    "comment": "",
                    "address_line": "",
                    "zip_code": "",
                    "country": {
                        "id": "AR",
                        "name": "Argentina"
                    },
                    "state": {
                        "id": "AR-C",
                        "name": "Capital Federal"
                    },
                    "city": {
                        "id": "TUxBQkNPTDI3NDNa",
                        "name": "Colegiales"
                    },
                    "latitude": "",
                    "longitude": ""
                },
                "attributes": [
                    {
                        "source": 2860837171021627,
                        "id": "ITEM_CONDITION",
                        "value_id": "2230581",
                        "value_name": "Usado",
                        "attribute_group_id": "OTHERS",
                        "attribute_group_name": "Otros",
                        "name": "Condición del ítem",
                        "value_struct": null,
                        "values": [
                            {
                                "source": 2860837171021627,
                                "id": "2230581",
                                "name": "Usado",
                                "struct": null
                            }
                        ]
                    }
                ],
                "original_price": null,
                "category_id": "MLA412445",
                "official_store_id": null,
                "domain_id": "MLA-BOOKS",
                "catalog_product_id": null,
                "tags": [
                    "poor_quality_picture",
                    "immediate_payment",
                    "cart_eligible",
                    "shipping_guaranteed"
                ],
                "order_backend": 2,
                "use_thumbnail_id": true,
                "offer_score": null,
                "offer_share": null,
                "match_score": null,
                "winner_item_id": null,
                "melicoin": null
            },
            {
                "id": "MLA924254300",
                "site_id": "MLA",
                "title": "Ciencia Ficcion Utopia Y Mercado - Capanna Pablo Libro Nuevo",
                "seller": {
                    "id": 41459733,
                    "permalink": "http://perfil.mercadolibre.com.ar/DOLSOF",
                    "registration_date": "2007-11-07T22:20:05.000-04:00",
                    "car_dealer": false,
                    "real_estate_agency": false,
                    "tags": [
                        "normal",
                        "credits_profile",
                        "messages_as_seller"
                    ],
                    "seller_reputation": {
                        "power_seller_status": null,
                        "level_id": "5_green",
                        "metrics": {
                            "cancellations": {
                                "period": "365 days",
                                "rate": 0,
                                "value": 2
                            },
                            "claims": {
                                "period": "365 days",
                                "rate": 0,
                                "value": 2
                            },
                            "delayed_handling_time": {
                                "period": "365 days",
                                "rate": 0.0729,
                                "value": 7
                            },
                            "sales": {
                                "period": "365 days",
                                "completed": 122
                            }
                        },
                        "transactions": {
                            "canceled": 9,
                            "period": "historic",
                            "total": 131,
                            "ratings": {
                                "negative": 0.01,
                                "neutral": 0.02,
                                "positive": 0.97
                            },
                            "completed": 122
                        }
                    }
                },
                "price": 2980,
                "prices": {
                    "id": "MLA924254300",
                    "prices": [
                        {
                            "id": "4",
                            "type": "standard",
                            "amount": 2980,
                            "regular_amount": null,
                            "currency_id": "ARS",
                            "last_updated": "2022-02-02T02:47:28Z",
                            "conditions": {
                                "context_restrictions": [],
                                "start_time": null,
                                "end_time": null,
                                "eligible": true
                            },
                            "exchange_rate_context": "DEFAULT",
                            "metadata": {}
                        }
                    ],
                    "presentation": {
                        "display_currency": "ARS"
                    },
                    "payment_method_prices": [],
                    "reference_prices": [],
                    "purchase_discounts": []
                },
                "sale_price": null,
                "currency_id": "ARS",
                "available_quantity": 1,
                "sold_quantity": 4,
                "buying_mode": "buy_it_now",
                "listing_type_id": "gold_special",
                "stop_time": "2041-06-02T04:00:00.000Z",
                "condition": "used",
                "permalink": "https://articulo.mercadolibre.com.ar/MLA-924254300-ciencia-ficcion-utopia-y-mercado-capanna-pablo-libro-nuevo-_JM",
                "thumbnail": "http://http2.mlstatic.com/D_5215-MLA4309858998_052013-I.jpg",
                "thumbnail_id": "5215-MLA4309858998_052013",
                "accepts_mercadopago": true,
                "installments": {
                    "quantity": 12,
                    "amount": 422.59,
                    "rate": 70.17,
                    "currency_id": "ARS"
                },
                "address": {
                    "state_id": "AR-C",
                    "state_name": "Capital Federal",
                    "city_id": "TUxBQlJFVDgyMDVa",
                    "city_name": "Retiro"
                },
                "shipping": {
                    "free_shipping": false,
                    "mode": "me2",
                    "tags": [],
                    "logistic_type": "xd_drop_off",
                    "store_pick_up": false
                },
                "seller_address": {
                    "id": "",
                    "comment": "",
                    "address_line": "",
                    "zip_code": "",
                    "country": {
                        "id": "AR",
                        "name": "Argentina"
                    },
                    "state": {
                        "id": "AR-C",
                        "name": "Capital Federal"
                    },
                    "city": {
                        "id": "TUxBQlJFVDgyMDVa",
                        "name": "Retiro"
                    },
                    "latitude": "",
                    "longitude": ""
                },
                "attributes": [
                    {
                        "id": "ITEM_CONDITION",
                        "name": "Condición del ítem",
                        "value_struct": null,
                        "attribute_group_id": "OTHERS",
                        "attribute_group_name": "Otros",
                        "source": 1,
                        "value_id": "2230581",
                        "value_name": "Usado",
                        "values": [
                            {
                                "name": "Usado",
                                "struct": null,
                                "source": 1,
                                "id": "2230581"
                            }
                        ]
                    }
                ],
                "original_price": null,
                "category_id": "MLA412445",
                "official_store_id": null,
                "domain_id": "MLA-BOOKS",
                "catalog_product_id": null,
                "tags": [
                    "dragged_bids_and_visits",
                    "good_quality_picture",
                    "incomplete_technical_specs",
                    "immediate_payment",
                    "cart_eligible",
                    "shipping_guaranteed"
                ],
                "order_backend": 3,
                "use_thumbnail_id": true,
                "offer_score": null,
                "offer_share": null,
                "match_score": null,
                "winner_item_id": null,
                "melicoin": null
            }
        ],
        "sort": {
            "id": "relevance",
            "name": "Más relevantes"
        },
        "available_sorts": [
            {
                "id": "price_asc",
                "name": "Menor precio"
            },
            {
                "id": "price_desc",
                "name": "Mayor precio"
            }
        ],
        "filters": [
            {
                "id": "condition",
                "name": "Condición",
                "type": "text",
                "values": [
                    {
                        "id": "used",
                        "name": "Usado"
                    }
                ]
            }
        ],
        "available_filters": [
            {
                "id": "category",
                "name": "Categorías",
                "type": "text",
                "values": [
                    {
                        "id": "MLA412445",
                        "name": "Libros Físicos",
                        "results": 219955
                    },
                    {
                        "id": "MLA1367",
                        "name": "Antigüedades y Colecciones",
                        "results": 6222
                    },
                    {
                        "id": "MLA3043",
                        "name": "Comics e Historietas",
                        "results": 2091
                    },
                    {
                        "id": "MLA1955",
                        "name": "Revistas",
                        "results": 1888
                    },
                    {
                        "id": "MLA1168",
                        "name": "Música, Películas y Series",
                        "results": 1734
                    },
                    {
                        "id": "MLA1132",
                        "name": "Juegos y Juguetes",
                        "results": 1174
                    },
                    {
                        "id": "MLA1574",
                        "name": "Hogar, Muebles y Jardín",
                        "results": 969
                    },
                    {
                        "id": "MLA1368",
                        "name": "Arte, Librería y Mercería",
                        "results": 867
                    },
                    {
                        "id": "MLA1182",
                        "name": "Instrumentos Musicales",
                        "results": 663
                    },
                    {
                        "id": "MLA1500",
                        "name": "Construcción",
                        "results": 459
                    },
                    {
                        "id": "MLA1512",
                        "name": "Agro",
                        "results": 255
                    },
                    {
                        "id": "MLA455868",
                        "name": "Ebooks",
                        "results": 255
                    },
                    {
                        "id": "MLA1384",
                        "name": "Bebés",
                        "results": 153
                    },
                    {
                        "id": "MLA433385",
                        "name": "Catálogos",
                        "results": 153
                    },
                    {
                        "id": "MLA1039",
                        "name": "Cámaras y Accesorios",
                        "results": 102
                    },
                    {
                        "id": "MLA1499",
                        "name": "Industrias y Oficinas",
                        "results": 102
                    },
                    {
                        "id": "MLA1430",
                        "name": "Ropa y Accesorios",
                        "results": 102
                    },
                    {
                        "id": "MLA409431",
                        "name": "Salud y Equipamiento Médico",
                        "results": 102
                    },
                    {
                        "id": "MLA5726",
                        "name": "Electrodomésticos y Aires Ac.",
                        "results": 51
                    },
                    {
                        "id": "MLA3937",
                        "name": "Joyas y Relojes",
                        "results": 51
                    },
                    {
                        "id": "MLA1227",
                        "name": "Otros",
                        "results": 12907
                    },
                    {
                        "id": "MLA1953",
                        "name": "Otras categorías",
                        "results": 306
                    }
                ]
            },
            {
                "id": "state",
                "name": "Ubicación",
                "type": "text",
                "values": [
                    {
                        "id": "TUxBUENBUGw3M2E1",
                        "name": "Capital Federal",
                        "results": 135858
                    },
                    {
                        "id": "TUxBUEdSQWVmNTVm",
                        "name": "Bs.As. G.B.A. Oeste",
                        "results": 29133
                    },
                    {
                        "id": "TUxBUEdSQWU4ZDkz",
                        "name": "Bs.As. G.B.A. Norte",
                        "results": 28882
                    },
                    {
                        "id": "TUxBUEdSQXJlMDNm",
                        "name": "Bs.As. G.B.A. Sur",
                        "results": 25982
                    },
                    {
                        "id": "TUxBUFpPTmFpbnRl",
                        "name": "Buenos Aires Interior",
                        "results": 7956
                    },
                    {
                        "id": "TUxBUFNBTmU5Nzk2",
                        "name": "Santa Fe",
                        "results": 5103
                    },
                    {
                        "id": "TUxBUENPUmFkZGIw",
                        "name": "Córdoba",
                        "results": 4539
                    },
                    {
                        "id": "TUxBUENPU2ExMmFkMw",
                        "name": "Bs.As. Costa Atlántica",
                        "results": 3978
                    },
                    {
                        "id": "TUxBUE1FTmE5OWQ4",
                        "name": "Mendoza",
                        "results": 867
                    },
                    {
                        "id": "TUxBUFNBTno3ZmY5",
                        "name": "Santa Cruz",
                        "results": 612
                    },
                    {
                        "id": "TUxBUFLNT29iZmZm",
                        "name": "Río Negro",
                        "results": 357
                    },
                    {
                        "id": "TUxBUE5FVW4xMzMzNQ",
                        "name": "Neuquén",
                        "results": 306
                    },
                    {
                        "id": "TUxBUEVOVHMzNTdm",
                        "name": "Entre Ríos",
                        "results": 204
                    },
                    {
                        "id": "TUxBUFRVQ244NmM3",
                        "name": "Tucumán",
                        "results": 204
                    },
                    {
                        "id": "TUxBUENIQW8xMTNhOA",
                        "name": "Chaco",
                        "results": 153
                    },
                    {
                        "id": "TUxBUENPUnM5MjI0",
                        "name": "Corrientes",
                        "results": 153
                    },
                    {
                        "id": "TUxBUFNBTnM0ZTcz",
                        "name": "San Luis",
                        "results": 153
                    },
                    {
                        "id": "TUxBUENIVXQxNDM1MQ",
                        "name": "Chubut",
                        "results": 102
                    },
                    {
                        "id": "TUxBUEpVSnk3YmUz",
                        "name": "Jujuy",
                        "results": 102
                    },
                    {
                        "id": "TUxBUFNBTm9lOTlk",
                        "name": "Santiago del Estero",
                        "results": 102
                    },
                    {
                        "id": "TUxBUE1JU3MzNjIx",
                        "name": "Misiones",
                        "results": 51
                    },
                    {
                        "id": "TUxBUENBVGFiY2Fm",
                        "name": "Catamarca",
                        "results": 51
                    },
                    {
                        "id": "TUxBUEZPUmE1OTk5",
                        "name": "Formosa",
                        "results": 51
                    },
                    {
                        "id": "TUxBUExBWmEyNzY0",
                        "name": "La Rioja",
                        "results": 51
                    }
                ]
            },
            {
                "id": "price",
                "name": "Precio",
                "type": "range",
                "values": [
                    {
                        "id": "*-450.0",
                        "name": "Hasta $ 450",
                        "results": 73217
                    },
                    {
                        "id": "450.0-1000.0",
                        "name": "$450 a $1.000",
                        "results": 93163
                    },
                    {
                        "id": "1000.0-*",
                        "name": "Más de $1.000",
                        "results": 83773
                    }
                ]
            },
            {
                "id": "accepts_mercadopago",
                "name": "Filtro por MercadoPago",
                "type": "boolean",
                "values": [
                    {
                        "id": "yes",
                        "name": "Con MercadoPago",
                        "results": 250153
                    }
                ]
            },
            {
                "id": "installments",
                "name": "Pago",
                "type": "text",
                "values": [
                    {
                        "id": "yes",
                        "name": "En cuotas",
                        "results": 250102
                    },
                    {
                        "id": "no_interest",
                        "name": "Sin interés",
                        "results": 10969
                    }
                ]
            },
            {
                "id": "shipping",
                "name": "Tipo de entrega",
                "type": "text",
                "values": [
                    {
                        "id": "mercadoenvios",
                        "name": "Con envío",
                        "results": 242452
                    }
                ]
            },
            {
                "id": "power_seller",
                "name": "Filtro por calidad de vendedores",
                "type": "boolean",
                "values": [
                    {
                        "id": "yes",
                        "name": "Mejores vendedores",
                        "results": 67636
                    }
                ]
            },
            {
                "id": "since",
                "name": "Filtro por fecha de comienzo",
                "type": "text",
                "values": [
                    {
                        "id": "today",
                        "name": "Publicados hoy",
                        "results": 612
                    }
                ]
            },
            {
                "id": "until",
                "name": "Filtro por fecha de finalización",
                "type": "text",
                "values": [
                    {
                        "id": "today",
                        "name": "Finalizan hoy",
                        "results": 204
                    }
                ]
            },
            {
                "id": "has_video",
                "name": "Filtro por publicaciones con video",
                "type": "boolean",
                "values": [
                    {
                        "id": "yes",
                        "name": "Publicaciones con video",
                        "results": 1020
                    }
                ]
            },
            {
                "id": "has_pictures",
                "name": "Filtro por publicaciones con imágenes",
                "type": "boolean",
                "values": [
                    {
                        "id": "yes",
                        "name": "Con fotos",
                        "results": 249847
                    }
                ]
            },
            {
                "id": "adult_content",
                "name": "Filtro de contenido adulto",
                "type": "boolean",
                "values": [
                    {
                        "id": "yes",
                        "name": "Contenido adulto",
                        "results": 310
                    }
                ]
            },
            {
                "id": "shipping_cost",
                "name": "Costo de envío",
                "type": "text",
                "values": [
                    {
                        "id": "free",
                        "name": "Gratis",
                        "results": 5509
                    }
                ]
            },
            {
                "id": "FORMAT",
                "name": "Formato",
                "type": "STRING",
                "values": [
                    {
                        "id": "2132698",
                        "name": "Papel",
                        "results": 168250
                    },
                    {
                        "id": "2431740",
                        "name": "Físico",
                        "results": 14550
                    },
                    {
                        "id": "2132699",
                        "name": "Digital",
                        "results": 300
                    }
                ]
            },
            {
                "id": "ITEM_CONDITION",
                "name": "Condición",
                "type": "STRING",
                "values": [
                    {
                        "id": "2230581",
                        "name": "Usado",
                        "results": 250150
                    }
                ]
            },
            {
                "id": "LANGUAGE",
                "name": "Idioma",
                "type": "STRING",
                "values": [
                    {
                        "id": "313886",
                        "name": "Español",
                        "results": 143750
                    },
                    {
                        "id": "313885",
                        "name": "Inglés",
                        "results": 22650
                    },
                    {
                        "id": "313883",
                        "name": "Francés",
                        "results": 3650
                    },
                    {
                        "id": "313889",
                        "name": "Italiano",
                        "results": 1400
                    },
                    {
                        "id": "313884",
                        "name": "Alemán",
                        "results": 1250
                    },
                    {
                        "id": "1258229",
                        "name": "Portugués",
                        "results": 650
                    }
                ]
            },
            {
                "id": "MIN_RECOMMENDED_AGE",
                "name": "Edad mínima recomendada",
                "type": "range",
                "values": [
                    {
                        "id": "(*-4años)",
                        "name": "Menos de 4 años",
                        "results": 21700
                    },
                    {
                        "id": "[4años-10años)",
                        "name": "4 a 9 años",
                        "results": 15050
                    },
                    {
                        "id": "[10años-13años)",
                        "name": "10 a 12 años",
                        "results": 16150
                    },
                    {
                        "id": "[13años-16años)",
                        "name": "13 a 15 años",
                        "results": 15450
                    },
                    {
                        "id": "[16años-*)",
                        "name": "16 años o más",
                        "results": 17200
                    }
                ]
            },
            {
                "id": "NARRATION_TYPE",
                "name": "Tipo de narración",
                "type": "STRING",
                "values": [
                    {
                        "id": "7488907",
                        "name": "Manual",
                        "results": 56050
                    },
                    {
                        "id": "7488906",
                        "name": "Novela",
                        "results": 43300
                    },
                    {
                        "id": "7488905",
                        "name": "Cuento",
                        "results": 18850
                    },
                    {
                        "id": "7488908",
                        "name": "Poesía",
                        "results": 4150
                    }
                ]
            }
        ]
    }
"""
}
