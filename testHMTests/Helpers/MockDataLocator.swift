//
//  MockDataLocator.swift
//  testHMTests
//
//  Created by Hugo Castrejon on 14/02/22.
//

import Foundation

/// Implements the basic attributes to build a mock class
internal protocol Mockeable {
    /// A valid JSON string that repesents mock data to an specific structure
    var jsonValue: String { get }
}

/// Implements mock data locator functionalities
internal protocol MockDataLocatorProtocol {
    /// Locates json mock data from a valid class that implements MockProtocol
    /// - Parameter mock: A valid instance of mock class that implements MockProtocol
    func locate(from mock: Mockeable) -> Data?
}

class MockDataLocator: MockDataLocatorProtocol {
    func locate(from mock: Mockeable) -> Data? {
        return mock.jsonValue.data(using: .utf8)
    }
}
