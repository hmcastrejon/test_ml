//
//  DataExtension.swift
//  testHMTests
//
//  Created by Hugo Castrejon on 14/02/22.
//

import Foundation
extension Data {
    func decode<T: Decodable>(_ type: T.Type,
                              from data: Data,
                              dateDecodingStrategy: JSONDecoder.DateDecodingStrategy = .deferredToDate,
                              keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy = .useDefaultKeys) -> T? {
        let decoder = JSONDecoder()
        guard let decodedData = try? decoder.decode(T.self, from: data) else {
            #if DEBUG
                print("⚠️ Could not decode data for type \(type)")
            #endif
            return nil
        }

        return decodedData
    }
}
